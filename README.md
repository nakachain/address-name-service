# address-name-service
Contract that resolves human readable names and translates to native addresses

[![pipeline status](https://gitlab.com/nakachain/address-name-service/badges/master/pipeline.svg)](https://gitlab.com/nakachain/address-name-service/commits/master)
[![coverage report](https://gitlab.com/nakachain/address-name-service/badges/master/coverage.svg)](https://gitlab.com/nakachain/address-name-service/commits/master)
